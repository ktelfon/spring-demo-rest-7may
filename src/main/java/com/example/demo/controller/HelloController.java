package com.example.demo.controller;

import com.example.demo.Printer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private Printer printer;

    public HelloController(Printer printer) {
        this.printer = printer;
    }

    @GetMapping("/hello")
    public String getHello(){
        return printer.getHello();
    }




}
