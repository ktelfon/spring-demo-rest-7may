package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    private UserRepo userRepo;

    public UserController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping("/user/create")
    public String createRandomUser() {
        User user = new User();
        user.setAge(10);
        user.setName("John");
        userRepo.save(user);
        return "Ok";
    }

    @GetMapping("/user/all")
    public List<User> getAll() {
        return userRepo.findAll();
    }

    @GetMapping("/user/all/{age}")
    public List<User> getByAge(@PathVariable("age") Integer age) {
        return userRepo.findAllByAge(age);
    }

    @GetMapping("/user/{id}")
    public User getById(@PathVariable("id") Long id) {
        return userRepo.findById(id).get();
    }

    @PostMapping("/user/save")
    public User save(@RequestBody User user) {
        return userRepo.save(user);
    }

    @PutMapping("/user/update/{id}")
    public User update(@PathVariable("id") Long id, @RequestBody User user) {
        User userFromDb = userRepo.findById(id).orElseThrow(() -> new NullPointerException("User not found."));
        user.setId(userFromDb.getId());
        return userRepo.save(user);
    }

    @DeleteMapping("/user/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        User userFromDb = userRepo.findById(id).orElseThrow(() -> new NullPointerException("User not found."));
        userRepo.delete(userFromDb);
        return "Deleted.";
    }
}
